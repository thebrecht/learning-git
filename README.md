# Git 版本控管介紹 

## 基本觀念
- 版本控管
- Git

## Git 基本指令

### 環境設定
設定你的git在發佈時，所使用的名稱和email
> git config --global user.name "your-name"
> git config --global user.email your-email

在不同的專案，也可以各別設定名稱和email(在專案目錄底下執行)
> git config user.name your-name
> git config user.email your-email

讀取設定，只需要省略設定值即可
> git config user.name 
> git config user.email 


### 建立Git專案
先建立專案要使用的目錄，或是到既有的目錄底下，執行初始化 Git 專案
> git init

### Git 做了什麼?
- 建立本地端的版本庫(.git目錄)

### Git的主要概念
- working directory (工作目錄)
- index
- repository

### 版本控管的狀態變化
![File Life Cycle](http://git-scm.com/book/en/v2/book/02-git-basics/images/lifecycle.png)

【新增檔案】

#### untracked (未追蹤，刪除後無法復原)
> echo hello world  >  hellogit.txt # windows請自行開啟文字編輯器編輯
> git status

#### staged (暫存、加入索引)
> git add hellogit.txt #或是 git add .
> git status

#### unmodified (加入版本庫)
> git commit -m 'first init'
> git status
> git log


【修改檔案】
#### modified (修改檔案，與版本庫產生差異)
> 將 hello world 改成 hello git
> git status

#### staged (暫存、加入索引)
> git git add .
> git status

#### 同一個檔案具備 modified 和 staged 狀態
> 將 hello git 改成 hello Git
> git status

#### unmodified (加入版本庫)
> git commit -m 'change world to git'
> git status
> git log

【狀態的反向切換】
*未add 前*
> 修改檔案，加入第２行 hello everyone!
> git checkout -- hellogit.txt

*未commit 前*
> 修改檔案，加入第２行 hello everyone!
> git add .
> git reset HEAD hellogit.txt


*commit 後*
> git add .
> git commit -m '加入第2行'

- 放棄commit，退回到staged 狀態

> git reset --soft HEAD^ #退回staged狀態
> git log
> git status #查看狀態一下
> git reset ORIG_HEAD #復原到退回前，Crtl+z的概念


- 放棄commit，退回到modified 狀態(未add前）

> git reset HEAD^ #退回modified狀態,等同於 git reset --mixed HEAD
> git log
> git status #查看狀態一下
> git reset ORIG_HEAD #復原到退回前，Crtl+z的概念


- 放棄commit，退回到上一個commit

> git reset --hard HEAD^ #退回上一個commit
> git log
> git status #查看狀態一下
> git reset ORIG_HEAD #復原到退回前，Crtl+z的概念


## 建立branch 分支
分支就是切換到另一個複制版本，可以盡情修改，不會影響到原來的東西
> git branch #查看分支
> git branch new-branch-name #建立新分支
> git checkout new-branch-name #切換到新分支
> git checkout -b new-branch-name #上面2動合一
> git branch -d branch-name  #刪除分支

### 建立新分支並commit新檔
> git checkout -b develop
> 修改 hellogit.txt 加入README.md
> git add .
> git commit -m 'add README.md'
> git log

### 切回master
> git checkout master
> git log

### 合併分支
> git merge develop

## 處理衝突
> 在 README.md 加入第一行文字 Learning Git is so happy
> git add .
> git commit -m 'add title'
> git checkout develop
> 在 README.md 加入第一行文字 #Learning Git is so cool, I like it. 
> git add .
> git commit -m 'add header'
> git checkout master
> git merge develop
> 衝突發生，編輯衝突


## 遠端專案的互動
> git clone 
> git push origin master
> git pull origin master


## 學習資源
- http://backlogtool.com/git-guide/tw/
- https://github.com/doggy8088/Learn-Git-in-30-days
- http://www-cs-students.stanford.edu/~blynn/gitmagic/intl/zh_tw/

update 3/13

